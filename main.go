package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/elliotchance/pie/pie"
	"github.com/knakk/sparql"
)

type Anime struct {
	ID        string      `json:"id"`
	Title     string      `json:"title,omitempty"`
	Picture   string      `json:"picture,omitempty"`
	Thumbnail string      `json:"thumbnail,omitempty"`
	Season    string      `json:"season,omitempty"`
	Year      uint16      `json:"year,omitempty"`
	Episodes  uint16      `json:"episodes,omitempty"`
	Type      string      `json:"type,omitempty"`
	Status    string      `json:"status,omitempty"`
	Sources   pie.Strings `json:"sources,omitempty"`
	Synonyms  pie.Strings `json:"synonyms,omitempty"`
	Tags      pie.Strings `json:"tags,omitempty"`
	Relations pie.Strings `json:"relations,omitempty"`
}

type AnimeQuery struct {
	ID         string
	Title      string
	Season     string
	Status     string
	Type       string
	Year       string
	TagQueries string
	Synonym    string
}

func BuildTagsQuery(tags []string) string {
	if len(tags) == 0 {
		return ""
	}

	const filterFormat = `
	FILTER EXISTS {
		?id schema:keywords %s .
		FILTER (%s)
	}
	`
	const keywordFormat = "REGEX(%s, \"%s\", \"i\")"

	var objs string
	var regexes string

	for i, tag := range tags {
		currentObj := "?tags" + strconv.FormatInt(int64(i+2), 10)
		objs = objs + currentObj
		regex := fmt.Sprintf(keywordFormat, currentObj, tag)
		regexes = regexes + regex
		if i < len(tags)-1 {
			objs = objs + ", "
			regexes = regexes + " && "
		}
	}
	return fmt.Sprintf(filterFormat, objs, regexes)
}

func MapSparqlResult(result *sparql.Results) ([]Anime, error) {
	animeMap := make(map[string]*Anime)
	rb := result.Results.Bindings
	for _, b := range rb {
		id := b["id"].Value

		if animeMap[id] != nil {
			anime := animeMap[id]
			if sources := b["sources"].Value; sources != "" {
				if !anime.Sources.Contains(sources) {
					anime.Sources = anime.Sources.Append(sources)
				}
			}
			if synonyms := b["synonyms"].Value; synonyms != "" {
				if !anime.Synonyms.Contains(synonyms) {
					anime.Synonyms = anime.Synonyms.Append(synonyms)
				}
			}
			if tags := b["tags"].Value; tags != "" {
				if !anime.Tags.Contains(tags) {
					anime.Tags = anime.Tags.Append(tags)
				}
			}
			if relations := b["relations"].Value; relations != "" {
				if !anime.Relations.Contains(relations) {
					anime.Relations = anime.Relations.Append(relations)
				}
			}
		} else {
			anime := &Anime{}
			anime.ID = id
			anime.Title = b["title"].Value
			anime.Picture = b["picture"].Value
			anime.Season = b["season"].Value
			anime.Type = b["type"].Value
			anime.Status = b["status"].Value
			if epStr := b["episodes"].Value; epStr != "" {
				episodes, err := strconv.ParseUint(epStr, 10, 16)
				if err == nil {
					anime.Episodes = uint16(episodes)
				}
			}
			if yearStr := b["year"].Value; yearStr != "" {
				year, err := strconv.ParseUint(yearStr, 10, 16)
				if err == nil {
					anime.Year = uint16(year)
				}
			}
			if sources := b["sources"].Value; sources != "" {
				anime.Sources = anime.Sources.Append(sources)
			}
			if synonyms := b["synonyms"].Value; synonyms != "" {
				anime.Synonyms = anime.Synonyms.Append(b["synonyms"].Value)
			}
			if tags := b["tags"].Value; tags != "" {
				anime.Tags = anime.Tags.Append(b["tags"].Value)
			}
			if relations := b["relations"].Value; relations != "" {
				anime.Relations = anime.Relations.Append(b["relations"].Value)
			}
			animeMap[id] = anime
		}
	}

	var animes []Anime
	for _, anime := range animeMap {
		animes = append(animes, *anime)
	}

	return animes, nil
}

func main() {
	sparqlEndpoint := os.Getenv("SPARQL_ENDPOINT")
	if sparqlEndpoint == "" {
		sparqlEndpoint = "http://localhost:3030/tmdb/sparql"
	}

	repo, err := sparql.NewRepo(sparqlEndpoint, sparql.Timeout(120*time.Second))
	if err != nil {
		log.Fatal(err)
	}

	f, err := os.Open("main_query.rq")
	if err != nil {
		log.Fatal(err)
	}
	bank := sparql.LoadBank(f)

	http.HandleFunc(
		"/search", func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Header().Set("Access-Control-Allow-Methods", "DELETE")
			w.Header().Set(
				"Access-Control-Allow-Headers",
				"Accept, Content-Type, Content-Length,"+
					" Accept-Encoding, X-CSRF-Token, Authorization",
			)
			queries := r.URL.Query()
			log.Println(queries.Get("id"))
			animeQuery := AnimeQuery{
				ID:         queries.Get("id"),
				Title:      queries.Get("title"),
				Season:     queries.Get("season"),
				Status:     queries.Get("status"),
				Type:       queries.Get("type"),
				Year:       queries.Get("year"),
				TagQueries: BuildTagsQuery(queries["tag"]),
				Synonym:    queries.Get("synonym"),
			}
			if searchQuery := queries.Get("search"); searchQuery != "" && animeQuery == (AnimeQuery{}) {
				animeQuery = AnimeQuery{
					Title: searchQuery,
				}
			}
			query, err := bank.Prepare("anime-query", animeQuery)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				_, _ = fmt.Fprintf(w, "{\"err\": \"%v\"}", err)
				return
			}

			result, err := repo.Query(query)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				_, _ = fmt.Fprintf(w, "{\"err\": \"%v\"}", err)
				return
			}

			animes, err := MapSparqlResult(result)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				_, _ = fmt.Fprintf(w, "{\"err\": \"%v\"}", err)
				return
			}

			b, err := json.Marshal(animes)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				_, _ = fmt.Fprintf(w, "{\"err\": \"%v\"}", err)
				return
			}
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			_, _ = w.Write(b)
		},
	)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	log.Printf("listening at 0.0.0.0:%s", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf("0.0.0.0:%s", port), nil))
}
