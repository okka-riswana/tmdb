const source = require("./anime-offline-database/anime-offline-database.json");
const uuid = require("uuid");
const fs = require("fs");

const data = source.data;

const context = {
  tmdb: "http://tmdb.bakalo.li/",
  schema: "http://schema.org/",
  animes: {
    "@id": "tmdb:animeList",
    "@type": "tmdb:Anime",
  },
  title: "schema:name",
  episodes: "schema:numberOfEpisodes",
  sources: {
    "@id": "schema:sameAs",
    "@type": "@id",
  },
  synonyms: "schema:alternateName",
  picture: "schema:image",
  thumbnail: "schema:thumbnailUrl",
  tags: "schema:keywords",
  animeSeason: "tmdb:animeSeason",
  season: {
    "@id": "tmdb:seasonName",
    "@type": "tmdb:SeasonName",
  },
  year: "schema:datePublished",
  relations: {
    "@id": "tmdb:relatedTo",
    "@type": "@id",
  },
  status: {
    "@id": "tmdb:publicationStatus",
    "@type": "tmdb:PublicationStatus",
  },
  type: {
    "@id": "tmdb:publicationType",
    "@type": "tmdb:PublicationType",
  },
};

const processData = (anime) => {
  const id = `tmdb:${uuid.v4().split("-")[0]}`;
  return {
    ...anime,
    "@id": id,
    "@type": "tmdb:Anime",
    animeSeason: {
      ...anime.animeSeason,
      "@id": id,
    },
  };
};

(async () => {
  const processedData = {
    "@context": context,
    animes: data.map(processData),
  };

  const animeList = processedData.animes;
  const numOfAnimes = 10;
  const animeWithRelations = [];

  const findRelated = (anime) => {
    return animeList.filter((anime2) =>
      anime.relations.some((r) => anime2.sources.includes(r))
    );
  };

  const pushWithRelated = (anime) => {
    const exists = animeWithRelations.find((a) => a["@id"] === anime["@id"]);
    if (exists) {
      return;
    }
    const relateds = findRelated(anime);
    animeWithRelations.push({
      ...anime,
      relations: relateds.map((anime) => anime["@id"]),
    });
    relateds.forEach(pushWithRelated);
  };

  for (let i = 0; i < numOfAnimes; i++) {
    const anime1 = animeList[i];
    console.log(`processing anime [${i + 1}/${numOfAnimes}]: ${anime1["@id"]}`);

    pushWithRelated(anime1);
  }

  console.log("total anime:", animeWithRelations.length);

  const relatedData = {
    "@context": context,
    animes: animeWithRelations,
  };

  fs.writeFileSync("../anime-data.jsonld", JSON.stringify(relatedData));
})();
