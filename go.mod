module gitlab.com/okka-riswana/tmdb

go 1.16

require (
	github.com/elliotchance/pie v1.38.2 // indirect
	github.com/knakk/digest v0.0.0-20160404164910-fd45becddc49 // indirect
	github.com/knakk/rdf v0.0.0-20190304171630-8521bf4c5042 // indirect
	github.com/knakk/sparql v0.0.0-20200914044602-05f8b3d450be
)
