# syntax=docker/dockerfile:1
FROM golang:1.16 AS builder
WORKDIR /go/src/gitlab.com/okka-riswana/tmdb/
COPY . .
RUN go get -d -v .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o bin/tmdb .

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /app/
COPY --from=builder /go/src/gitlab.com/okka-riswana/tmdb/bin .
COPY main_query.rq .
CMD ["/app/tmdb"]